# Annix **Davis Sanders Homes** Frontend

###### Project's content:

* `/css` contains compiled css
* `/fonts` contains fonts
* `/img` contains images
* `/js` contains javascript
* `/html` contains compiled html
* `/pug` contains `pug` (preprocessor for `html`) used for better development
* `/sass` contains `sass` (preprocessor for `css`) used for better development

###### To run locally:

* Install node.js on your machine https://nodejs.org/en/.

* Run sudo `npm i -g live-server pug-cli`.

* Run `npm i`.

* Run `npm start`.

###### To build project after changes

* Run `npm run pug` - cancel the command when all html compiled

* Run `npm run build` to compile css

