
//--- Bolder text on hover

$('.bold-fade').each(function () {
    $(this).attr('data-title', $(this).find('span').text());
});

//--- End


//--- Navbar

const wWidth = $(window).width();

const header = $('.header');
const hDrop = $('.header__dropdown');
const hDropToggle = $('.header__dropdown a.dropdown-toggle');
const hCtaMd = $('.header__links__bottom, .header__cta--md');
const doc = $('body, html');

$('.header__menu').click(function () {
    $(this).toggleClass('active');
    header.toggleClass('opened');
    doc.toggleClass('non-scrollable');
    $('.header__links__bottom').slideToggle();
});

if (wWidth > 1025) {
    hDrop.removeClass('clickable');
} else {
    hDrop.addClass('clickable');
    hDropToggle.removeAttr('href');
}

$(window).resize(function () {
    const wWidth = $(window).width();
    if (wWidth > 1025) {
        hCtaMd.show();
        hDrop.removeClass('clickable');
        $('.dropdown-content').hide();
        if (header.hasClass('opened')) {
            doc.removeClass('non-scrollable');
            $(this).removeClass('active');
        }
    } else {
        hDropToggle.removeAttr('href');
        hDrop.addClass('clickable');
        if (!header.hasClass('opened')) {
            hCtaMd.hide();
        }
    }

});

hDrop.click(function () {
    if ($(this).hasClass('clickable')) {
        $(this).find('.dropdown-toggle').toggleClass('active');
        $(this).find('.dropdown-content').slideToggle();
    }
});


//--- End


//--- Hero section height on mobile and tablets

function mFlexHeight() {
    const wHeight = $(window).innerHeight();
    $('.hero').height(wHeight);
}

mFlexHeight();

$(window).resize(function () {
    mFlexHeight();
});

//--- End


//--- Filter form

const filterItem = $('.filters__form__item');
const dropToggle = $('.dropdown-toggle');
const dropContent = $('.dropdown-content');
const filters = $('.filters');

$(window).resize(function () {
    const wWidth = $(window).width();
    if (wWidth > 1009) {
        filterItem.addClass('dropdown');
        filters.show();
    } else {
        filterItem.removeClass('dropdown');
    }
});

if (wWidth > 1025) {
    filterItem.addClass('dropdown');
} else {
    filterItem.removeClass('dropdown');
}

$('.filters__form__item .dropdown-toggle, .main-items .dropdown-toggle').click(function () {
    if ($(this).parent().hasClass('dropdown')) {
        dropContent.not($(this).parent().find('.dropdown-content')).slideUp();
        dropToggle.not($(this)).removeClass('active');
        $(this).toggleClass('active').parent().find('.dropdown-content').slideToggle();
    }
});

$(document).mouseup(function(e) {
    const container = $('.dropdown-content, .dropdown-toggle');
    if (container.parent().hasClass('dropdown')) {
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            dropContent.slideUp();
            dropToggle.removeClass('active');
        }
    }
});

// Radios

const labelEstate = $('label#estate');
const labelSuperB = $('label#superb');

$('input:checked').parent().addClass('checked');

$('input:radio').change(function(){
    if($(this).is(":checked")) {
        $(this).parents().eq(2).find('.checked').removeClass('checked');
    }
});

$('input:radio, input:checkbox').change(function(){
    if($(this).is(":checked")) {
        $(this).parent().addClass('checked');
    } else {
        $(this).parent().removeClass('checked');
    }
});

// For IE
$('label.radio img').click(function () {
    $(this).parent().click();
});
// End

$('.search-by label.radio input').change(function(){
    if ($(this).hasClass('super-b')) {
        labelSuperB.show();
        labelEstate.hide();
    } else if ($(this).hasClass('estate')) {
        labelSuperB.hide();
        labelEstate.show();
    }
});

$('.radios-inline label input').change(function(){
    if($(this).is(":checked")) {
        const thisVal = $(this).val();
        $(this).parents().eq(3).find('.text-small').text(thisVal);
    }
});

$('.filters__show__btn').click(function () {
    filters.fadeIn();
    doc.addClass('non-scrollable');
});

$('.filters__close__btn').click(function () {
    filters.fadeOut();
    doc.removeClass('non-scrollable');
});

//--- End


//--- Sort
$('.main-items__header__sort .dropdown-content li').click(function () {
    $(this).parent().find('li.active').removeClass('active');
    $(this).addClass('active');
    $(this).parent().prev().removeClass('active');
    $(this).parent().fadeOut();
    $(this).parents().eq(3).find('span.title-small').text($(this).text());
});
//--- End


//--- Main card

$('.main-card__sizes span').click(function () {
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
});


$('.main-card__footer__preview').click(function () {

    const thisPreview = $(this).parents().eq(3).find('.preview');
    const thisPreviewHeight = thisPreview.height();
    const thisItem = $(this).parents().eq(3);
    const container = $('.main-items.section .container');
    const wWidth = $(window).width();
    const body = $('html, body');

    $('.preview').not(thisPreview).fadeOut();
    $('.main-card__footer__preview').not($(this)).removeClass('active');

    $('.main-items__item').css({
        'margin-bottom': 0
    });

    thisPreview.fadeToggle();
    $(this).toggleClass('active');

    if ($(this).hasClass('active')) {
        thisItem.css({
            'margin-bottom': thisPreviewHeight + 110
        });
        if (thisItem.length === 0) {
            container.css({
                'margin-bottom': thisPreviewHeight + 100
            })
        } else {
            container.css({
                'margin-bottom': 0
            })
        }

        if (wWidth < 767) {
            thisItem.css({
                'margin-bottom': thisPreviewHeight + 55
            });

            body.stop().animate({scrollTop: $(this).offset().top - 50}, 500, 'swing', function(){});
        } else {
            body.stop().animate({scrollTop: $(this).offset().top}, 500, 'swing', function(){});
        }

    } else {
        thisItem.css({
            'margin-top': 0
        });
        container.css({
            'margin-bottom': 0
        });
    }

    const thisPosY = $(this).offset().top;
    const rowPosY = $('.main-items__row').offset().top;
    const thisHeight = $(this).height();

    thisPreview.css({
        'top': thisPosY - rowPosY + thisHeight + 30
    });


});

//--- End


//--- Overview page

$('.accord-title').click(function () {
    $(this).parent().find('.accord-body').slideToggle();

    const thisI = $(this).find('i');
    thisI.text(thisI.text() == "+" ? "-" : "+");
});

//Tabs

if (wWidth > 767) {
    $('.tab-content' + '#' + $('.tabs li.active').attr('data-content')).addClass('active-tab');
}

$('.tabs li').click(function () {
    const thisContent = $(this).attr('data-content');
    const thisWidth = $(this).outerWidth();
    const thisLeft = $(this).offset().left;
    const thisParentLeft = $(this).parent().offset().left;
    $(this).parent().find('.tabs__nav').css({
        'width': thisWidth,
        'left': thisLeft-thisParentLeft
    });
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
    $('.tab-content').hide();
    $('.tab-content' + '#' + thisContent).show();
});

$('.accordion-title').click(function () {
    $(this).toggleClass('active');
    $(this).next().slideToggle();
});

$('.preview__header__sizes li').click(function () {
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
});

//--- End


//--- Popup

$(document).mouseup(function(e) {
    const container = $('.popup__form');
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $('.popup').fadeOut();
    }
});

$('.popup__close').click(function () {
    $('.popup').fadeOut();
});

//--- End


//--- F.A.Q.

$('.faq__accord__item.active').find('.faq__accord__item__body').show();

$('.faq__accord__item__title').click(function () {
    const thisItem = $(this).parent();
    thisItem.parent().find('.active').not(thisItem).removeClass('active');
    thisItem.toggleClass('active');
    thisItem.parent().find('.faq__accord__item__body').not($(this).next()).slideUp();
    $(this).next().slideToggle();
});

//--- END


//--- For selectric

$('.selectric').on('selectric-open', function() {
    $(this).parents().eq(2).addClass('active');
}).on('selectric-close', function() {
    $(this).parents().eq(2).removeClass('active');
});

$('.selectric').on('click', function () {
    if ($(this).parents().eq(2).hasClass('active')) {
        $(this).selectric('close');
    }
});

//--- End

