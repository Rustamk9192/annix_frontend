const express = require('express'),
    port = 9000,
    app = express();

app.use(express.static(__dirname + '/'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/html/index.html');
});

app.get('/home-design', (req, res) => {
    res.sendFile(__dirname + '/html/home-design.html');
});

app.get('/house-land', (req, res) => {
    res.sendFile(__dirname + '/html/house-land.html');
});

app.get('/overview', (req, res) => {
    res.sendFile(__dirname + '/html/overview.html');
});

app.get('/display-homes', (req, res) => {
    res.sendFile(__dirname + '/html/display-homes.html');
});

app.get('/office', (req, res) => {
    res.sendFile(__dirname + '/html/office.html');
});

app.get('/build-with-us', (req, res) => {
    res.sendFile(__dirname + '/html/build-with-us.html');
});

app.get('/estate', (req, res) => {
    res.sendFile(__dirname + '/html/estate.html');
});

app.get('/for-sale', (req, res) => {
    res.sendFile(__dirname + '/html/for-sale.html');
});

app.get('/contact', (req, res) => {
    res.sendFile(__dirname + '/html/contact.html');
});

app.listen(port, () => {
    console.info(`> Server is running on port: ${port}`);
});
